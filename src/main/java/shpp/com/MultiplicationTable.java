package shpp.com;

import java.io.IOException;

public class MultiplicationTable implements Constants {

    private int[] dataInteger;
    private long[] dataLong;
    private double[] dataDouble;

    private static final String ERROR_MESSAGE = "The product of the minimum and maximum number is out of bounds " +
            "for the specified type. Please select the correct multiplier type!!";

    private static final String MESSAGE_PATTERN = "{} * {} = {}";

    /**
     * The constructor initializes the input data, runs its validation, and casts the string to the appropriate type
     *
     * @throws IOException
     * @throws MyException
     */
    public MultiplicationTable() throws IOException, MyException {
        InputData inputData = new InputData();
        inputData.validationProperties();
        if (inputData.getVarType().equals("int")) {
            this.dataInteger = inputData.stringToInt();
        } else if (inputData.getVarType().equals("long")) {
            this.dataLong = inputData.stringToLong();
        } else {
            this.dataDouble = inputData.stringToDouble();
        }
    }

    /**
     * The method chooses, depending on the type of variables, which table to output to the console.
     * If the maximum result of multiplication goes beyond the limits of the maximum value for this type,
     * the method throws an exception.
     *
     * @throws MyException - result exceeding the maximum value for the selected type
     */
    public void chooseTable() throws MyException {
        if (dataInteger != null) {
            if ((Integer.MAX_VALUE / dataInteger[0]) > dataInteger[1] ||
                    (Integer.MAX_VALUE / dataInteger[1]) > dataInteger[0]) {
                printTable(dataInteger[0], dataInteger[1], dataInteger[2]);
            } else {
                throw new MyException(ERROR_MESSAGE);
            }
        } else if (dataLong != null) {
            if ((Long.MAX_VALUE / dataLong[0]) > dataLong[1] ||
                    (Long.MAX_VALUE / dataLong[1]) > dataLong[0]) {
                printTable(dataLong[0], dataLong[1], dataLong[2]);
            } else {
                throw new MyException(ERROR_MESSAGE);
            }
        } else if (dataDouble != null) {
            if ((Double.MAX_VALUE / dataDouble[0]) > dataDouble[1] ||
                    (Double.MAX_VALUE / dataDouble[1]) > dataDouble[0]) {
                printTable(dataDouble[0], dataDouble[1], dataDouble[2]);
            } else {
                throw new MyException(ERROR_MESSAGE);
            }
        }
    }

    /**
     * The method multiplies two numbers starting from the minimum number and going with
     * the increment step to its maximum value. All multiplication results are output to the console.
     *
     * @param min - minimum multiplier
     * @param max - maximum increment
     * @param increment - increment
     */
    private void printTable(int min, int max, int increment) {
        for (int i = min; i <= max; i += increment) {
            int multiplication = min * i;
            logger.info(MESSAGE_PATTERN, min, i, multiplication);
        }
    }

    /**
     * The method multiplies two numbers starting from the minimum number and going with
     * the increment step to its maximum value. All multiplication results are output to the console.
     *
     * @param min - minimum multiplier
     * @param max - maximum increment
     * @param increment - increment
     */
    private void printTable(long min, long max, long increment) {
        for (long i = min; i <= max; i += increment) {
            long multiplication = min * i;
            logger.info(MESSAGE_PATTERN, min, i, multiplication);
        }
    }

    /**
     * The method multiplies two numbers starting from the minimum number and going with
     * the increment step to its maximum value. All multiplication results are output to the console.
     *
     * @param min - minimum multiplier
     * @param max - maximum increment
     * @param increment - increment
     */
    private void printTable(double min, double max, double increment) {
        for (double i = min; i <= max; i += increment) {
            double multiplication = min * i;
            logger.info(MESSAGE_PATTERN, min, i, multiplication);
        }
    }
}
