package shpp.com;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader implements Constants {

    /**
     * The method loads data from the properties file into a buffer. The method works when running the program
     * from IDEA and external properties from Jar.
     * @param fileName - properties filename
     * @return - link to properties buffer
     * @throws IOException
     */
    public Properties loadProperties(String fileName){

        Properties properties = new Properties();

        try {
            logger.info("Opening a property from a Jar!");
            InputStream rootPath = MyApp.class.getClassLoader().getResourceAsStream(fileName);
            properties.load(rootPath);
            return properties;
        } catch (Exception e) {
            logger.debug("Opening properties from IDEA!");
            try (FileInputStream inputStream = new FileInputStream("src/main/resources/"+ fileName)) {
                properties.load(inputStream);
            } catch (IOException exception) {
                logger.error("Something wrong !!!");
            }
            return properties;
        }
    }

}
