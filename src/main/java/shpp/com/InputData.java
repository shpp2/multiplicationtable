package shpp.com;

import java.io.IOException;
import java.util.Properties;

public class InputData implements Constants {

    private String varType;
    private final String minNum;
    private final String maxNum;
    private final String increment;
    private static final String EXCEPTION_MESSAGE_EXIST_PROPERTY = "Error! Pleas write minNum in app.properties!";
    private static final String EXCEPTION_MESSAGE = "Incorrect values of the increment or min - max numbers!";

    /**
     * The constructor initializes reading the properties file and getting the values of the variables for minNum,
     * maxNum, increment
     */
    public InputData() throws IOException {
        PropertiesLoader loader = new PropertiesLoader();
        Properties properties = loader.loadProperties(PROPERTIES_FILE_NAME);
        this.minNum = properties.getProperty(PROPERTY_KEY_MIN_NUM);
        this.maxNum = properties.getProperty(PROPERTY_KEY_MAX_NUM);
        this.increment = properties.getProperty(PROPERTY_KEY_INCREMENT);
    }

    /**
     * The method checks whether the type of the system variable matches the type of the value in Properties
     * @throws MyException - throws out in case of a mismatch between the type of the system variable and the
     *                          value of the fields in properties
     */
    public void validationProperties() throws MyException {
        checkingExistPropertyValues();
        validationSystemProperties();
        if (!this.varType.equals("double") && (this.minNum.contains(".") ||
                this.maxNum.contains(".") || this.increment.contains("."))) {
            throw new MyException("ERROR!  The type of the variable does not match its value!");
        }
    }

    /**
     * The method checks for the presence of corresponding keys and their values in the properties file.
     * @throws MyException - throws out if any of the required keys or values are missing in properties
     */
    private void checkingExistPropertyValues() throws MyException {
        if (this.minNum == null) {
            throw new MyException(EXCEPTION_MESSAGE_EXIST_PROPERTY);
        }
        if (this.maxNum == null) {
            throw new MyException(EXCEPTION_MESSAGE_EXIST_PROPERTY);
        }
        if (this.increment == null) {
            throw new MyException(EXCEPTION_MESSAGE_EXIST_PROPERTY);
        }
    }

    /**
     * The method verifies the system parameter for the type of variables and,
     * in its absence, assigns the default type - int.
     */
    private void validationSystemProperties() {

        String type = System.getProperty(SYSTEMS_PROPERTY);

        if (type != null) {
            if (type.equals("int") || type.equals("short") || type.equals("byte") ||
                    type.equals("long")) {
                this.varType = "long";
            } else if (type.equals("double") || type.equals("float")) {
                this.varType = "double";
            } else {
                logger.info("You have entered an incorrect value for a system variable, " +
                        "it has been assigned an int by default!");
                this.varType = "int";
            }
        } else {
            logger.info("You did not enter a value for a system variable, it is assigned to int by default!");
            this.varType = "int";
        }
    }

    /**
     * The method converts the property values minNum, maxNum, increment
     * to long type and returns these values in an array
     *
     * @return - array of long with values minNum, maxNum, increment
     */
    public long[] stringToLong() throws MyException {
        long minNumber = Long.parseLong(this.minNum);
        long maxNumber = Long.parseLong(this.maxNum);
        long incrementNumber = Long.parseLong(this.increment);
        if (incrementNumber >= maxNumber || minNumber >= maxNumber) {
            throw new MyException(EXCEPTION_MESSAGE);
        }
        return new long[]{minNumber, maxNumber, incrementNumber};
    }

    /**
     * The method converts the property values minNum, maxNum, increment
     * to int type and returns these values in an array
     *
     * @return - array of int with values minNum, maxNum, increment
     */
    public int[] stringToInt() throws MyException {
        int minNumber = Integer.parseInt(this.minNum);
        int maxNumber = Integer.parseInt(this.maxNum);
        int incrementNumber = Integer.parseInt(this.increment);
        if (incrementNumber >= maxNumber || minNumber >= maxNumber) {
            throw new MyException(EXCEPTION_MESSAGE);
        }
        return new int[]{minNumber, maxNumber, incrementNumber};
    }

    /**
     * The method converts the property values minNum, maxNum, increment
     * to double type and returns these values in an array
     *
     * @return - array of doubles with values minNum, maxNum, increment
     */
    public double[] stringToDouble() throws MyException {
        double minNumber = Double.parseDouble(this.minNum);
        double maxNumber = Double.parseDouble(this.maxNum);
        double incrementNumber = Double.parseDouble(this.increment);
        if (incrementNumber >= maxNumber || minNumber >= maxNumber) {
            throw new MyException(EXCEPTION_MESSAGE);
        }
        return new double[]{minNumber, maxNumber, incrementNumber};
    }

    /**
     * The method returns the number type for the variable
     *
     * @return - the number type for the variable
     */
    public String getVarType() {
        return this.varType;
    }
}
