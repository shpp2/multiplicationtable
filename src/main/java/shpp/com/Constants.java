package shpp.com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface Constants {
    String PROPERTIES_FILE_NAME = "app.properties";
    String SYSTEMS_PROPERTY = "varType";
    String PROPERTY_KEY_MIN_NUM = "minNum";
    String PROPERTY_KEY_MAX_NUM = "maxNum";
    String PROPERTY_KEY_INCREMENT = "increment";
    Logger logger = LoggerFactory.getLogger("logback");

}
