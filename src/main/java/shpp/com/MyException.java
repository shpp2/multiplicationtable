package shpp.com;

public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
