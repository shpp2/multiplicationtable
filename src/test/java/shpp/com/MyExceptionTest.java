package shpp.com;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MyExceptionTest {

    @Test
    void testMyExceptionThrowException() {
        MyException exception = new MyException("exception");
        assertThrows(MyException.class, () -> {throw exception;});
    }
}
