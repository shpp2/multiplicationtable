package shpp.com;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class InputDataTest implements Constants {

    @Test
    void testValidationPropertiesDoesNotThrowExceptionWhenInit() {
        assertDoesNotThrow(() -> {new InputData();});
    }

    @Test
    void testStringToLongMatchValuesInAnArrayWhenValuesInPropertyIsLongType() throws IOException, MyException {
        InputData data = new InputData();
        long[] expected = data.stringToLong();
        logger.info("{}, {}, {}", expected[0], expected[1], expected[2]);
        long[] actual = new long[]{10, 10000, 100};
        assertEquals(expected[0], actual[0]);
        assertEquals(expected[1], actual[1]);
        assertEquals(expected[2], actual[2]);
    }

    @Test
    void testStringToIntMatchValuesInAnArrayWhenValuesInPropertyIsIntType() throws IOException, MyException {
        InputData data = new InputData();
        int[] expected = data.stringToInt();
        logger.info("{}, {}, {}", expected[0], expected[1], expected[2]);
        int[] actual = new int[]{10, 10000, 100};
        assertEquals(expected[0], actual[0]);
        assertEquals(expected[1], actual[1]);
        assertEquals(expected[2], actual[2]);
    }

    @Test
    void testStringToDoubleMatchValuesInAnArrayWhenValuesInPropertyIsDoubleType() throws MyException, IOException {
        InputData data = new InputData();
        double[] expected = data.stringToDouble();
        logger.info("{}, {}, {}", expected[0], expected[1], expected[2]);
        double[] actual = new double[]{10.0, 10000.0, 100.0};
        assertEquals(expected[0], actual[0]);
        assertEquals(expected[1], actual[1]);
        assertEquals(expected[2], actual[2]);
    }

    @Test
    void testGetVarTypeReturnNullWhenVarTypeIsDefault() throws IOException {
        InputData data = new InputData();
        String expected = data.getVarType();
        assertNull(expected);
    }
}