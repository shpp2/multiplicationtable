package shpp.com;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTableTest {

    @Test
    void chooseTableDoesNotThrowExceptionWhenItRun() throws MyException, IOException {
        MultiplicationTable table = new MultiplicationTable();
        assertDoesNotThrow(table::chooseTable);
    }
}