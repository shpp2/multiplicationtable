package shpp.com;

import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesLoaderTest implements Constants {

    @Test
    void testPropertiesLoaderNotNullWhenInit(){
        PropertiesLoader loader = new PropertiesLoader();
        Properties expected = loader.loadProperties(PROPERTIES_FILE_NAME);
        assertNotNull(expected);
    }
}